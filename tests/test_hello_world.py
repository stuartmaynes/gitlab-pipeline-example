from helloworld.greetings import shout

def test_shout():
    assert shout() == 'HELLO WORLD!'
